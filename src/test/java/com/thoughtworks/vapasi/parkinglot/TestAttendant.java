package com.thoughtworks.vapasi.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;


import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;

public class TestAttendant {

    public static final int MAX_SIZE = 3;
    private String uniquelocationIdentifier="ParkingLot##0";

    @Test
    public void shouldParkCarInParkingLot() {
        Car car = new Car(2369);
        ParkingLot mockedParkingLot = Mockito.mock(ParkingLot.class);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(mockedParkingLot);
        Attendant attendant = new Attendant(parkingLots);

        attendant.park(car);
        Mockito.verify(mockedParkingLot, Mockito.times(1)).park(car);
    }

    @Test
    public void shouldUnparkCarInParkingLot() {
        Car car = new Car(2369);
        ParkingLot lot1 = new ParkingLot(MAX_SIZE, uniquelocationIdentifier);

        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(lot1);
        Attendant attendant = new Attendant(parkingLots);

        attendant.park(car);
        attendant.unpark(car.getPlateNumber());

        Assertions.assertThrows(NoCarParked.class, () -> {
            attendant.unpark(car.getPlateNumber());
        });
    }


    @Test
    public void shouldThrowCarNotFoundException() {
        Car parkCar = new Car(2369);
        Car unPackCar = new Car(7000);
        List<ParkingLot> parkingLots = new ArrayList<>();
        ParkingLot parkingLot = new ParkingLot(MAX_SIZE, uniquelocationIdentifier);
        parkingLots.add(parkingLot);
        Attendant attendant = new Attendant(parkingLots);

        attendant.park(parkCar);

        Assertions.assertThrows(NoCarParked.class, () -> {
            attendant.unpark(unPackCar.getPlateNumber());
        });
    }


    @Test
    public void shouldManageMultipleParkinglots() {
        Car mockCar = Mockito.mock(Car.class);
        ParkingLot lot1 = Mockito.mock(ParkingLot.class);
        ParkingLot lot2 = Mockito.mock(ParkingLot.class);

        List<ParkingLot> parkings = new ArrayList<>();
        parkings.add(lot1);
        parkings.add(lot2);
        Attendant attendant = new Attendant(parkings);

        attendant.park(mockCar);
        attendant.park(mockCar);
        attendant.park(mockCar);
        attendant.park(mockCar);
        attendant.park(mockCar);

        Mockito.verify(lot1, Mockito.times(3)).park(mockCar);
        Mockito.verify(lot2, Mockito.times(2)).park(mockCar);

    }

    @Test
    public void shouldThrowParkingFullException() {
        ParkingLot lot1 = new ParkingLot(MAX_SIZE, uniquelocationIdentifier);
        List<ParkingLot> parkings = new ArrayList<>();
        parkings.add(lot1);
        Attendant attendant = new Attendant(parkings);
        attendant.park(new Car(7000));
        attendant.park(new Car(7002));
        attendant.park(new Car(7003));

        Assertions.assertThrows(ParkingLotFullException.class, () -> {
            attendant.park(new Car(7004));
        });

    }


    @Test
    public void shouldAllowParkingAfterUnparkingFullParkingLot() {
        ParkingLot lot1 = new ParkingLot(MAX_SIZE, uniquelocationIdentifier);
        List<ParkingLot> parkings = new ArrayList<>();
        parkings.add(lot1);
        Attendant attendant = new Attendant(parkings);
        attendant.park(new Car(7000));
        attendant.park(new Car(7002));
        Car car = new Car(7003);
        attendant.park(car);
        attendant.unpark(car.getPlateNumber());

        Assertions.assertDoesNotThrow(() -> {
            attendant.park(car);
        });
    }

    @Test
    public void shouldThrowParkingFullExceptionForCarsParkedFromMultipleWays() {
        ParkingLot lot1 = new ParkingLot(1, uniquelocationIdentifier);
        ParkingLot lot2 = new ParkingLot(1, uniquelocationIdentifier);
        ParkingLot lot3 = new ParkingLot(1, uniquelocationIdentifier);
        List<ParkingLot> parkings = new ArrayList<>();
        parkings.add(lot1);
        parkings.add(lot2);
        parkings.add(lot3);
        Attendant attendant = new Attendant(parkings);
        lot2.park(new Car(2));
        attendant.park(new Car(1));
        attendant.park(new Car(3));
        Assertions.assertThrows(ParkingLotFullException.class, () -> {
            attendant.park(new Car(4));
        });

    }

    @Test
    public void shouldHandleIndexOutofBound()
    {
        ParkingLot lot1 = new ParkingLot(2, uniquelocationIdentifier);
        ParkingLot lot2 = new ParkingLot(1, uniquelocationIdentifier);
        List<ParkingLot> parkings = new ArrayList<>();
        parkings.add(lot1);
        parkings.add(lot2);
        Attendant attendant = new Attendant(parkings);
        attendant.park(new Car(1));
      //  attendant.park(new Car(2));

        lot2.park(new Car(2));
        attendant.park( new Car(3));

    }

    @Test
    public void shouldEvenlyDistributeCarsAmongParkingLots()
    {
        ParkingLot lot1 = new ParkingLot(5, uniquelocationIdentifier, new Car(1),new Car(2));
        ParkingLot lot2 = new ParkingLot(5, uniquelocationIdentifier, new Car(3),new Car(4),new Car(5));
        ParkingLot lot3 = new ParkingLot(5, uniquelocationIdentifier, new Car(6));

        List<ParkingLot> parkings = new ArrayList<>();
        parkings.add(lot1);
        parkings.add(lot2);
        parkings.add(lot3);
        Attendant attendant = new Attendant(parkings,new EvenlyDistributionStrategy());

        attendant.park(new Car(1001));
        Assertions.assertTrue( lot3.isCarPresent(1001));
    }

    @Test
    public void shouldParkCarInParkingLotWithMaximumFreeSpace()
    {
        ParkingLot lot1 = new ParkingLot(7, uniquelocationIdentifier, new Car(1),new Car(2));
        ParkingLot lot2 = new ParkingLot(10, uniquelocationIdentifier, new Car(3),new Car(4),new Car(5));
        ParkingLot lot3 = new ParkingLot(3, uniquelocationIdentifier, new Car(6));

        List<ParkingLot> parkings = new ArrayList<>();
        parkings.add(lot1);
        parkings.add(lot2);
        parkings.add(lot3);
        Attendant attendant = new Attendant(parkings, new MaxFreeSpaceStrategy());

        attendant.park(new Car(1001));
        Assertions.assertTrue( lot2.isCarPresent(1001));
    }



}
