package com.thoughtworks.vapasi.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalTime;
import java.util.List;
import java.util.Map;

public class TestPoliceDepartment {

//    @Test
//    public void shouldInformPoliceDepartmentaboutCarPark()
//    {
//
//        Car car =new Car(1,CarColour.BLACK,CarBrand.HUNDAI);
//        ParkedCarInfo parkedCarInfo=Mockito.mock(ParkedCarInfo.class);
//        Mockito.when(parkedCarInfo.getCar()).thenReturn(car);
//
//        ParkingLot parkingLot1=new ParkingLot(5, "ParkingLot##1");
//        PoliceDeparment mockedPoliceDeparment=Mockito.mock(PoliceDeparment.class);
//        parkingLot1.addSubscriber(mockedPoliceDeparment);
//        parkingLot1.park(car);
////        ParkedCarInfo parkedCarInfo=new ParkedCarInfo(car,"ParkingLot##1",1,LocalTime.now());
//        Mockito.verify(mockedPoliceDeparment,Mockito.times(1)).carParked(parkedCarInfo);
//    }

    @Test
    public void shouldReturnOneWhiteCarForSingleLocation()
    {
        Car car=new Car(1);
        ParkingLot parkingLot1=new ParkingLot(5, "ParkingLot##1");
        PoliceDeparment policeDeparment=new PoliceDeparment();
        parkingLot1.addSubscriber(policeDeparment);
        parkingLot1.park(new Car(1));
        parkingLot1.park(new Car(2));
        parkingLot1.park(new Car(3,CarColour.WHITE,CarBrand.MERCEDES));
        List<ParkedCarInfo> carLocationList=policeDeparment.findLocationForCars(CarColour.WHITE);
        Assertions.assertEquals(1,carLocationList.size());
    }

    @Test
    public void shouldReturnSpecifiedCarWithLocations()
    {
        ParkingLot parkingLot1=new ParkingLot(5, "ParkingLot##1");
        PoliceDeparment policeDeparment=new PoliceDeparment();
        parkingLot1.addSubscriber(policeDeparment);
        parkingLot1.park(new Car(1));
        parkingLot1.park(new Car(2));
        parkingLot1.park(new Car(3,CarColour.WHITE,CarBrand.MERCEDES));
        parkingLot1.park(new Car(3,CarColour.WHITE,CarBrand.TOYOTA));


        ParkingLot parkingLot2=new ParkingLot(5, "ParkingLot##2");
        parkingLot2.addSubscriber(policeDeparment);
        parkingLot2.park(new Car(4));
        parkingLot2.park(new Car(5));
        parkingLot2.park(new Car(6,CarColour.WHITE,CarBrand.TOYOTA));

        List<ParkedCarInfo> carLocationList=policeDeparment.findLocationForCars(CarColour.WHITE);

        Assertions.assertEquals(3,carLocationList.size());

    }

    @Test
    public void shouldReturnCarsWithSpecificMake()
    {
        ParkingLot parkingLot1=new ParkingLot(5, "ParkingLot##1");
        PoliceDeparment policeDeparment=new PoliceDeparment();
        parkingLot1.addSubscriber(policeDeparment);
        parkingLot1.park(new Car(1001));
        parkingLot1.park(new Car(2001));
        parkingLot1.park(new Car(3002,CarColour.WHITE,CarBrand.MERCEDES));
        parkingLot1.park(new Car(3333,CarColour.WHITE,CarBrand.TOYOTA));

        List<ParkedCarInfo> carLocationList=policeDeparment.findCars(CarBrand.TOYOTA);
        Assertions.assertEquals(CarBrand.TOYOTA,carLocationList.get(0).getCar().getBrand());
    }

    @Test
    public  void shouldReturnCarsWithSpecificColourAndMake()
    {
        ParkingLot parkingLot1=new ParkingLot(5, "ParkingLot##1");
        PoliceDeparment policeDeparment=new PoliceDeparment();
        parkingLot1.addSubscriber(policeDeparment);
        parkingLot1.park(new Car(1001));
        parkingLot1.park(new Car(2001));
        parkingLot1.park(new Car(3002,CarColour.WHITE,CarBrand.MERCEDES));
        parkingLot1.park(new Car(3333,CarColour.WHITE,CarBrand.TOYOTA));

        List<ParkedCarInfo> carLocationList=policeDeparment.findCars(CarColour.WHITE,CarBrand.TOYOTA);
        Assertions.assertEquals(CarBrand.TOYOTA,carLocationList.get(0).getCar().getBrand());

    }

    @Test
    public  void shouldReturnCarsParkedInLast30Mins()
    {
        ParkingLot parkingLot1=new ParkingLot(5, "ParkingLot##1");
        PoliceDeparment policeDeparment=new PoliceDeparment();
        parkingLot1.addSubscriber(policeDeparment);
        parkingLot1.park(new Car(1001));
        parkingLot1.park(new Car(2001));
        parkingLot1.park(new Car(3002,CarColour.WHITE,CarBrand.MERCEDES));
        parkingLot1.park(new Car(3333,CarColour.WHITE,CarBrand.TOYOTA));

        List<ParkedCarInfo> carsParkedInLast30Mins=policeDeparment.findCarsParkedInLast30Mins();
        Assertions.assertEquals(4,carsParkedInLast30Mins.size());
    }

}
