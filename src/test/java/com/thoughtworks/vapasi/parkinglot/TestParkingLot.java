package com.thoughtworks.vapasi.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;


public class TestParkingLot {

    public static final int MAX_SIZE =3;
    private String uniquelocationIdentifier="PUNE";

    @Test
    public void shouldPark()
    {
        ParkingLot parkinglot=new ParkingLot(MAX_SIZE, uniquelocationIdentifier);
        Car car =new Car(2369);
        parkinglot.park(car);
        Assertions.assertEquals(car,parkinglot.unpark(car.getPlateNumber()));
    }

    @Test
    public void shouldParkUnpark()
    {
        ParkingLot parkinglot= mock(ParkingLot.class);
        Car car=new Car(2369);
        parkinglot.park(car);
        parkinglot.unpark(car.getPlateNumber());
        Mockito.verify(parkinglot,Mockito.times(1)).unpark(car.getPlateNumber());
    }

    @Test
    public void shouldThrowNoCarExceptionOnNotFindingSpecificCar()
    {
        ParkingLot parkinglot=new ParkingLot(MAX_SIZE, uniquelocationIdentifier);
        Car car=new Car(2369);
        Assertions.assertThrows(NoCarParked.class, () -> {
            parkinglot.unpark(car.getPlateNumber());;
        });
    }

    @Test
    public  void shouldThrowParkingFullException()
    {
        ParkingLot parkinglot=new ParkingLot(MAX_SIZE, uniquelocationIdentifier);
        Car car1=new Car(2369);
        Car car2=new Car(2360);
        Car car3=new Car(2361);
        Car car4=new Car(2362);
        parkinglot.park(car1);
        parkinglot.park(car2);
        parkinglot.park(car3);
        Assertions.assertThrows(ParkingLotFullException.class, () -> {
            parkinglot.park(car4);;
        });


    }

    @Test
    public void shouldNotifyParkingLotListenersOnParkingFull()
    {
        ParkingLot parkinglot=new ParkingLot(1, uniquelocationIdentifier);
        Car car1=new Car(2369);
        IParkingListener parkingListener = mock(IParkingListener.class);
        parkinglot.addSubscriber(parkingListener);
        parkinglot.park(car1);
        Mockito.verify(parkingListener, times(1)).parkingfull(parkinglot);
    }

    @Test
    public void shouldNotifyParkingLotListenersOnlyWhenFirstCarIsUnParkedAfterFull()
    {
        ParkingLot parkinglot=new ParkingLot(2, uniquelocationIdentifier);
        IParkingListener parkingListener = mock(IParkingListener.class);
        parkinglot.addSubscriber(parkingListener);

        parkinglot.park(new Car(2369));
        parkinglot.park(new Car(2370));
        parkinglot.unpark(2369);

        Mockito.verify(parkingListener, times(1)).parkingSpaceAvailable(parkinglot);
    }

    @Test
    public void shouldNotNotifyParkingLotListenersWhenSecondCarIsUnParked()
    {
        ParkingLot parkinglot=new ParkingLot(2, uniquelocationIdentifier);
        IParkingListener parkingListener = mock(IParkingListener.class);
        parkinglot.addSubscriber(parkingListener);

        parkinglot.park(new Car(2369));
        parkinglot.park(new Car(2370));
        parkinglot.unpark(2369);

        parkinglot.unpark(2370);

        Mockito.verify(parkingListener, times(1)).parkingSpaceAvailable(parkinglot);
    }

}
