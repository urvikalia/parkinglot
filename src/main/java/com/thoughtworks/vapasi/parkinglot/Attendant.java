package com.thoughtworks.vapasi.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class Attendant implements IParkingListener {

    private List<ParkingLot> availableParkingLot = new ArrayList<>();
    private List<ParkingLot> fullParkingLot = new ArrayList<>();

    private ParkingDistributionStrategy parkingDistributionStrategy;

    public Attendant(List<ParkingLot> parkingLot, ParkingDistributionStrategy parkingDistributionStrategy) {
        this.availableParkingLot = parkingLot;
        this.parkingDistributionStrategy = parkingDistributionStrategy;
        for (ParkingLot lot : availableParkingLot)
            lot.addSubscriber(this);
    }

    public Attendant(List<ParkingLot> parkingLot) {
        this(parkingLot,new RoundRobinStrategy() );
    }

    public void park(Car car) {
        throwParkingLotFullException();
        parkingDistributionStrategy.next(availableParkingLot).park(car);
    }


    public void unpark(int carNumber) {
        ParkingLot parkingLot = findParkingLotForCar(carNumber);
        if (parkingLot == null) {
            throw new NoCarParked();
        }
        parkingLot.unpark(carNumber);

    }

    @Override
    public void parkingfull(ParkingLot parkingLot) {
        availableParkingLot.remove(parkingLot);
        fullParkingLot.add(parkingLot);
    }

    @Override
    public void parkingSpaceAvailable(ParkingLot parkinglot) {
        fullParkingLot.remove(parkinglot);
        availableParkingLot.add(parkinglot);
    }

    private void throwParkingLotFullException() {
        if (availableParkingLot.size() == 0) {
            throw new ParkingLotFullException();
        }
    }

    private ParkingLot findParkingLotForCar(int carNumber) {
        List<ParkingLot> searchList = new ArrayList<>();
        searchList.addAll(fullParkingLot);
        searchList.addAll(availableParkingLot);
        for (ParkingLot curentParkinglot : searchList) {
            if (curentParkinglot.isCarPresent(carNumber))
                return curentParkinglot;
        }
        return null;
    }

}
