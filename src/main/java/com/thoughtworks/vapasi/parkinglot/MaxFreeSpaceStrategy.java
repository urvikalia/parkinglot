package com.thoughtworks.vapasi.parkinglot;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MaxFreeSpaceStrategy implements ParkingDistributionStrategy {

    @Override
    public ParkingLot next(List<ParkingLot> parkingLots) {
        ParkingLot maxFreeSpace = parkingLots.get(0);

        for (ParkingLot parkingLot: parkingLots) {
            if (parkingLot.getFreeSpace() > maxFreeSpace.getFreeSpace()) {
                maxFreeSpace = parkingLot;
            }
        }

        return maxFreeSpace;
    }
}
