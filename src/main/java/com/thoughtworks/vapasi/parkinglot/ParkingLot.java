package com.thoughtworks.vapasi.parkinglot;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class ParkingLot {

    private  final int MAX_SIZE;
    private final String uniqueLocationIdentifier;
    private List<Car> parkedCar = new ArrayList<>();

    private List<IParkingListener> parkingFullListeners=new ArrayList<>();
    private List<CarParkedListener> carParkedListeners=new ArrayList<>();

    public ParkingLot(int max_size) {
        this.MAX_SIZE = max_size;
        this.uniqueLocationIdentifier = "ParkingLotXX";
    }

    public ParkingLot(int max_size, String uniquelocationIdentifier) {
        this.MAX_SIZE = max_size;
        this.uniqueLocationIdentifier = uniquelocationIdentifier;
    }

    public ParkingLot(int MAX_SIZE, String uniquelocationIdentifier, Car... cars) {
        this(MAX_SIZE, uniquelocationIdentifier);
        for(Car car: cars)
            parkedCar.add(car);
    }

    public boolean park(Car car) throws ParkingLotFullException {

        if(!isParkingFull()) {
            parkedCar.add(car);
            informCarIsParked(car);
            if(isParkingFull())
                informParkingFull();
            return true;
        }
        else {
            throw new ParkingLotFullException();
        }

    }

    public Car unpark(int carNumber) {
        for(Car car:parkedCar)
        {
            if(car.getPlateNumber()==carNumber)
            {
                if(parkedCar.remove(car)) {
                    if(MAX_SIZE-1==parkedCar.size())
                    {
                        informParkingSpaceAvailable();
                    }
                    return car;
                }
            }
        }
        throw new NoCarParked();
    }

    public void addSubscriber(IParkingListener parkingListener) {
        parkingFullListeners.add(parkingListener);
    }

    public void addSubscriber(CarParkedListener carParkedListener) {
        carParkedListeners.add(carParkedListener);
    }


    public int getNumberOfParkedCars()
    {
        return parkedCar.size();
    }

    public int getFreeSpace()
    {
        return MAX_SIZE-getNumberOfParkedCars();
    }

   private void informCarIsParked(Car car)
   {
       ParkedCarInfo parkedCarInfo=new ParkedCarInfo(car, uniqueLocationIdentifier,parkedCar.size(), LocalTime.now());
       for(CarParkedListener carParkedListener:carParkedListeners)
           carParkedListener.carParked(parkedCarInfo);
   }

    private void informParkingFull()
    {
        for (IParkingListener listener:parkingFullListeners)
            listener.parkingfull(this);
    }

    private void informParkingSpaceAvailable()
    {
        for (IParkingListener listener:parkingFullListeners)
            listener.parkingSpaceAvailable(this);
    }

    private boolean isParkingFull() {
        return MAX_SIZE==parkedCar.size();
    }

    public boolean isCarPresent(int carNumber) {
        for(Car car:parkedCar)
        {
            if(car.getPlateNumber()==carNumber)
                return true;
        }
        return false;
    }
}
