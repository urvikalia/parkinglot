package com.thoughtworks.vapasi.parkinglot;

import java.util.List;

public interface ParkingDistributionStrategy {


    public ParkingLot next(List<ParkingLot> parkingLots);

}
