package com.thoughtworks.vapasi.parkinglot;

import java.util.List;

public class EvenlyDistributionStrategy implements ParkingDistributionStrategy {
    @Override
    public ParkingLot next(List<ParkingLot> parkingLots) {
        ParkingLot leastusedParkingLot = parkingLots.get(0);

        for(ParkingLot parkingLot:parkingLots)
        {
            if(parkingLot.getNumberOfParkedCars()<leastusedParkingLot.getNumberOfParkedCars())
            {
                leastusedParkingLot=parkingLot;
            }
        }
       return leastusedParkingLot;
    }
}
