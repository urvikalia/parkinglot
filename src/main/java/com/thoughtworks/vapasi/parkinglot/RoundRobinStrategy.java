package com.thoughtworks.vapasi.parkinglot;

import java.util.List;

public class RoundRobinStrategy implements ParkingDistributionStrategy {


    private int currentParkingLotIndex = 0;

    @Override
    public ParkingLot next(List<ParkingLot> parkingLots) {
        ParkingLot parkingLot=null;
        try {
            parkingLot=parkingLots.get(currentParkingLotIndex);

        } catch (IndexOutOfBoundsException indexOutOfBoundException) {
            currentParkingLotIndex = 0;
            parkingLot=parkingLots.get(currentParkingLotIndex);
        }
        moveToNextParkingLot(parkingLots);
        return parkingLot;

    }

    private void moveToNextParkingLot(List<ParkingLot> parkingLots) {
        currentParkingLotIndex++;
        if (currentParkingLotIndex >= parkingLots.size()) {
            currentParkingLotIndex = 0;
        }
    }

}
