package com.thoughtworks.vapasi.parkinglot;

import java.time.Duration;
import java.time.LocalTime;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PoliceDeparment implements CarParkedListener {

    List<ParkedCarInfo> parkedCars=new ArrayList<>();

    @Override
    public void carParked(ParkedCarInfo parkedCarInfo) {
            parkedCars.add(parkedCarInfo);
    }


    //TODO check if can refactor
    public List<ParkedCarInfo> findLocationForCars(CarColour carColour) {

        Map<String, List<ParkedCarInfo>> carMaps=parkedCars.stream().filter(parkedCarInfo -> carColour.equals(parkedCarInfo.getCar().getColour()))
                .collect(Collectors.groupingBy(ParkedCarInfo::getUniqueLocationIdentifier));
        carMaps.entrySet().stream().forEach(System.out::println);

       return  parkedCars.stream().filter(parkedCarInfo -> carColour.equals(parkedCarInfo.getCar().getColour()))
        .collect(Collectors.toList());



    }

    public List<ParkedCarInfo> findCars(CarBrand carBrand) {
        return parkedCars.stream()
                .filter(parkedCarInfo -> parkedCarInfo.getCar().getBrand().equals(carBrand))
                .collect(Collectors.toList());
    }

    public List<ParkedCarInfo> findCars(CarColour carColour, CarBrand carBrand) {
            return parkedCars.stream().filter(parkedCarInfo -> parkedCarInfo.getCar().getColour().equals(carColour))
                    .filter(parkedCarInfo -> parkedCarInfo.getCar().getBrand().equals(carBrand))
                    .collect(Collectors.toList());
    }

    public List<ParkedCarInfo> findCarsParkedInLast30Mins() {
        return parkedCars.stream().filter(parkedCarInfo ->
            ChronoUnit.MINUTES.between(LocalTime.now(),parkedCarInfo.getParkedTime())<=30)
            .collect(Collectors.toList());

    }

}
