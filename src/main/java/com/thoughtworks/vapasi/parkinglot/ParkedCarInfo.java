package com.thoughtworks.vapasi.parkinglot;

import java.time.LocalTime;
import java.util.Objects;

public class ParkedCarInfo {

    private Car car;
    private String uniqueLocationIdentifier;
    private int parkingSlotNumber;
    private LocalTime parkedTime;

    public ParkedCarInfo(Car car, String uniqueParkingLocationIdentifier, int parkingSlotNumber, LocalTime parkedTime) {
        this.car = car;
        this.uniqueLocationIdentifier = uniqueParkingLocationIdentifier;
        this.parkingSlotNumber = parkingSlotNumber;
        this.parkedTime = parkedTime;
    }

    public Car getCar() {
        return car;
    }

    public String getUniqueLocationIdentifier() {
        return uniqueLocationIdentifier;
    }

    public LocalTime getParkedTime() {
        return parkedTime;
    }

    @Override
    public String toString() {
        return "ParkedCarInfo{" +
                "car=" + car.toString() +
                ", uniqueLocationIdentifier='" + uniqueLocationIdentifier + '\'' +
                ", parkingSlotNumber=" + parkingSlotNumber +
                '}';
    }


}
