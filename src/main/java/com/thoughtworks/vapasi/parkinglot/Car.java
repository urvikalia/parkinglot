package com.thoughtworks.vapasi.parkinglot;


public class Car {

    private int plateNumber;
    private CarColour colour;
    private CarBrand brand;

    public Car(int carNumber) {
        this(carNumber,CarColour.BLACK,CarBrand.HUNDAI);
    }

    public Car(int plateNumber, CarColour colour, CarBrand brand) {
        this.plateNumber = plateNumber;
        this.colour = colour;
        this.brand = brand;
    }

    public int getPlateNumber() {
        return plateNumber;
    }

    public CarColour getColour() {
        return colour;
    }

    public CarBrand getBrand() {
        return brand;
    }

    @Override
    public String toString() {
        return "Car{" +
                "plateNumber=" + plateNumber +
                ", colour=" + colour +
                ", brand=" + brand +
                '}';
    }
}
