package com.thoughtworks.vapasi.parkinglot;

public enum Strategy {

    ROUND_ROBIN,EVEN_DISTRIBUTION,MAX_FREE_SPACE;

}
