package com.thoughtworks.vapasi.parkinglot;

public interface CarParkedListener {

    public void carParked(ParkedCarInfo parkedCarInfo);
}
